# API TEST TIVA NODE JS

Esta API foi projetada para atender os requisitos de um teste de uma fase de um processo seletivo de uma empresa chamada Tiva. A API fornece um catálogo de profissionais de um aplicação, onde é possível qualquer pessoa fazer um agendamento com os profissionais cadastrados.
Quando requisitado uma agenda disponível, o profissional escolhido recebe um alerta em tempo real via e-mail, assim como também a data, o horário e o e-mail do solicitante ficam armazenados no Calendário do Google do profissional, para implementar isto foi necessário integrar a API do Google Calendário v3.0.

---

### Instalação

1. Dê permissão de execução para o arquivo first.time.sh:

```shell
    sudo chmod +x ./src/scripts/run.all.sh
```

2. Execute o script, ele irá preparar todo o ambiente necessário usando docker (banco de dados, api e primeiro usuário admin):

   - Recomendável docker-compose version 1.27.4, as posteriores ainda se encontram com muitas issues abertas.

```shell
    ./src/scripts/run.all.sh
```

### Instalação: Outro método sem docker-compose

1. Dê permissão de execução para o arquivo run.db.sh:

```shell
    sudo chmod +x ./src/scripts/run.db.sh
```

2. Execute o script, ele irá preparar o banco postgres com o primeiro usuário usando docker:

```shell
    ./src/scripts/run.db.sh
```

3. Instale as dependências:

```shell
    npm install
```

ou

```shell
    yarn
```

4. Execute o projeto:

```shell
    npm run dev
```

ou

```shell
    yarn dev
```

---

### Rotas da API

## User

**METHOD - ROUTE - DESCRIPTION**

GET - /user/auth - auth user without authentication

GET - /user/list - list all users without authentication

GET - /user/data - return a user without authentication

GET - /user/first-user - create a user without authentication

POST - /user/create - create a user

PUT - /user/update - update a user

DELETE - /user/delete - delete a user

## Post - Referring a to user profile picture

**METHOD - ROUTE - DESCRIPTION**

POST - /post/create - create a user profile picture

GET - /post/list - list user profile pictures

DELETE - /user/delete - delete a user profile picture

## Address

**METHOD - ROUTE - DESCRIPTION**

POST - /address/create - create address linked to a user

GET - /address/list - list all addresses

POST - /address/data - return a address

PUT - /address/update - update a address

DELETE - /address/delete - delete a address

## Available Schedule

**METHOD - ROUTE - DESCRIPTION**

POST - /available-schedule/create - create available-schedule linked to a user

GET - /available-schedule/list - list all available-schedule

POST - /available-schedule/data - return a available-schedule

PUT - /available-schedule/update - update a available-schedule

DELETE - /available-schedule/delete - delete a available-schedule

## Schedule

**METHOD - ROUTE - DESCRIPTION**

POST - /schedule/create - create schedule linked to a user

GET - /schedule/list - list all schedule

POST - /schedule/data - return a schedule

PUT - /schedule/update - update a schedule

DELETE - /schedule/delete - delete a schedule

---

### Extras

Em caso de uso do software insomnia para teste de rotas, importar o arquivo **_Insomnia_Test_Tiva.json_** no insomnia.
