import { Request } from 'express'
import { Options } from 'multer'

type CustomRequest = Request & JwtPayload & {
    user: {
        id: string
        access_level: string
    } | undefined
}

type CustomRequestPost = CustomRequest & {
    file: {
        originalname: string
        size: number
        key: string
        location: string
    }
}

type CustomRequestAddress = Request & {
    body: {
        id: string
    }
}

type CustomFile = Express.Multer.File & {
    key?: string | string[]
}

type MulterConfig = MulterConfig & Options & {
    dest: string
    storageTypes: {
        local: multer.StorageEngine;
        s3: multer.StorageEngine;
    }
    limits: {
        fileSize: number
    }
    fileFilter: (req: Request, file: Express.Multer.File, cb: FileFilterCallback) => void
}

export {
    CustomRequest,
    CustomRequestPost,
    CustomRequestAddress,
    CustomFile,
    MulterConfig
}
