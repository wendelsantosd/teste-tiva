import { Request, Response , Router } from 'express'
import multer from 'multer'

import { addressController } from '@controllers/AddressController'
import { availableScheduleController } from '@controllers/AvailableScheduleController'
import { postController } from '@controllers/PostController'
import { scheduleController } from '@controllers/ScheduleController'
import { userController } from '@controllers/UserController'

import { version } from '../package.json'
import { verifyToken } from './middleware/verifyToken'
import { multerConfig } from './services/multer'

const router = Router()

router.get('/', (req: Request, res: Response) => {
    res.status(200).json(`API TEST TIVA - v${version} `)
})

router.get('/user/auth', userController.auth)
router.get('/user/list', userController.list)
router.get('/user/data', userController.data)
router.post('/user/first-user', userController.firstUser)

router.use(verifyToken)

router.post('/user/create', userController.create)
router.put('/user/update', userController.update)
router.delete('/user/delete', userController.delete)

router.post(
    '/post/create', 
    multer(multerConfig).single('file'),
    postController.create
)
router.get('/post/list', postController.list)
router.delete('/post/delete', postController.delete)

router.post('/address/create', addressController.create)
router.get('/address/list', addressController.list)
router.post('/address/data', addressController.data)
router.put('/address/update', addressController.update)
router.delete('/address/delete', addressController.delete)

router.post('/available-schedule/create', availableScheduleController.create)
router.get('/available-schedule/list', availableScheduleController.list)
router.post('/available-schedule/data', availableScheduleController.data)
router.put('/available-schedule/update', availableScheduleController.update)
router.delete('/available-schedule/delete', availableScheduleController.delete)

router.post('/schedule/create', scheduleController.create)
router.get('/schedule/list', scheduleController.list)
router.post('/schedule/data', scheduleController.data)
router.put('/schedule/update', scheduleController.update)
router.delete('/schedule/delete', scheduleController.delete)

export default router