import cors from 'cors'
import express, { json, urlencoded } from 'express'
import morgan from 'morgan'
import { serve, setup} from 'swagger-ui-express'

import { config }  from './config'
import { swagger } from './docs'
import router from './routes'


const app = express()

app.use(cors({
    origin: '*'
}))
app.use(json())
app.use(urlencoded({ extended: true }))
app.use(morgan('combined'))

app.use('/doc', serve, setup(swagger))

app.use(router)

app.listen(config.HTTP_SERVER_PORT, () => { console.log('Server is running!') })