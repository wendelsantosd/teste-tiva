echo '\nBulding the database\n'
sudo docker pull postgres
sudo docker run --name=postgres -p 5432:5432 -e POSTGRES_PASSWORD='docker' -e POSTGRES_USER='docker' -e POSTGRES_DB='db_docker' -d postgres:latest
sudo docker ps

sleep 3

echo '\nExecuta migrations no banco de dados\n'
npx prisma db push

sleep 3

echo -e '\n\nCadastra primeiro usuário admin\n'
curl -D - \
-H 'Accept: application/json' \
-H 'Content-Type: application/json' \
-X POST -d \
'{"name": "Wendel Santos","access_level": "admin","phone": "99999999999","email": "emailsendnode@gmail.com","hashed_password": "admin","description": "professional","address": {"zip": "66876289","state": "PA","city": "Belém","district": "Umarizal","street": "Alenor","number": "342"},"available_schedule": {"begin_date": "26/09/2021","end_date": "26/09/2021","begin_hour": "6:00","end_hour": "17:00"}}' \
'http://localhost:3333/user/first-user'