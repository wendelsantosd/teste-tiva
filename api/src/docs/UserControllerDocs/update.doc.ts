const update = {
    '/user/update': {
        put: {
            tags: [
                'User'
            ],
            summary: 'Update user',
            requestBody: {
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                id: {
                                    type: 'string'
                                },
                                name: {
                                    type: 'string'
                                },
                                access_level: {
                                    type: 'string'
                                },
                                phone: {
                                    type: 'string'
                                },
                                email: {
                                    type: 'string'
                                },
                                hashed_password: {
                                    type: 'string'
                                }
                            },
                            example: {
                                id: '2557fa48-484b-4ca1-944d-0a2b66e3845e',
                                name: 'Modified joao',
                                access_level: 'admin',
                                phone: '99999999999',
                                email: 'Modified joao@provider.com',
                                hashed_password: '123456'
                            }
                        }
                    }
                }
            },
            responses: {
                '200': {
                    content: {
                        'application/json': {
                            schema: {
                                type: 'string',
                                example: {
                                    message: 'user updated'
                                }
                            }
                        }
                    }
                },
                '404': {
                    content: {
                        'application/json': {
                            schema: {
                                type: 'string',
                                example: {
                                    message: 'user not found'
                                }
                            }
                        }
                    }
                },
                '500': {
                    content: {
                        'application/json': {
                            schema: {
                                type: 'string',
                                example: {
                                    message: '${error message}'
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

export { update }