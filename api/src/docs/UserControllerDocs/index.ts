import { auth } from './auth.doc'
import { create } from './create.doc'
import { remove } from './delete.doc'
import { list } from './list.doc'
import { update } from './update.doc'

const UserControllerDocs= { 
    ...auth,
    ...create,
    ...list,
    ...update,
    ...remove
}

export { UserControllerDocs}