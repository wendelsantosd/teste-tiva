const create = {
    '/user/create': {
        post: {
            tags: [
                'User'
            ],
            summary: 'Create user',
            requestBody: {
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                name: {
                                    type: 'string'
                                },
                                access_level: {
                                    type: 'string'
                                },
                                phone: {
                                    type: 'string'
                                },
                                email: {
                                    type: 'string'
                                },
                                hashed_password: {
                                    type: 'string'
                                }
                            },
                            example: {
                                name: 'joao',
                                access_level: 'admin',
                                phone: '99999999999',
                                email: 'joao@provider.com',
                                hashed_password: '123456'
                            }
                        }
                    }
                }
            },
            responses: {
                '201': {
                    content: {
                        'application/json': {
                            schema: {
                                type: 'string',
                                example: {
                                    message: 'user created'
                                }
                            }
                        }
                    }
                },
                '500': {
                    content: {
                        'application/json': {
                            schema: {
                                type: 'string',
                                example: {
                                    message: '${error message}'
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

export { create }