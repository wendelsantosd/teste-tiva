import { UserControllerDocs } from './UserControllerDocs'

const swagger = {
    openapi: '3.0.0',
    info: {
        title: 'Sensor Reading Service API Documentation',
        description: 'All API routes',
        version: '0.0.31',
        contact: {
            email: 'wendelwcsantos@gmail.com'
        }
    },
    paths: {
        ...UserControllerDocs
    }
}

export { swagger }