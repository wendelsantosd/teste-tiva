const convert = {
    dateStringToSplitDate: (date: string): string[] => {
        const splitedDate = date.split('/')

        return splitedDate
    },

    hoursStringToSplitHours: (hours: string): string[] => {
        const splitedHours = hours.split(':')

        return splitedHours
    }
}

export { convert }