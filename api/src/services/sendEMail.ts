import { createTransport } from 'nodemailer'
import smtpTransport from 'nodemailer-smtp-transport'

import { config } from '../config'

const sendEmail = async (
    to: string | undefined, 
    name: string,
    date: string, 
    hour: string,
    email: string
): Promise<void> => {
    const { user, pass } = config.NODEMAILER
    
    const tranporter = createTransport(
        smtpTransport({
            service: 'gmail',
            host: 'smtp.gmail.com',
            auth: {
                user,
                pass
            }
        })
    )

    const emailOptions = {
        from: user,
        to,
        subject: 'Notificação de novo agendamento',
        text: `Agendamento com ${name} marcado para a data ${date} às ${hour} horas. Email: ${email}
        `
    }

    await tranporter.sendMail(emailOptions, (err, payload) => {
        if (err) {
            console.log(err)
        } else {
            console.log(payload.response)
        }
    })
}

export default sendEmail
