import { google } from 'googleapis'

import { convert } from './convert'

const googleCalendar = async (
    date: string,
    begin_hour: string,
    end_hour: string,
    name: string,
    clientEmail: string,
    email: string | undefined
): Promise<string | void> => {
    const [day, month, year] = convert.dateStringToSplitDate(date)
    const [begin_hours, begin_minutes] = convert.hoursStringToSplitHours(begin_hour)
    const [end_hours, end_minutes] = convert.hoursStringToSplitHours(end_hour)

    const { OAuth2 } = google.auth

    const oAuth2Client = new OAuth2(
        '442338792034-vikpfsiqpkka89e1th5sphfov0frsuee.apps.googleusercontent.com',
        'SKbWE5El_h7pbMhhKSdAgN6D'
    )

    oAuth2Client.setCredentials({
        refresh_token:
    '1//04-LO3yY3jNiyCgYIARAAGAQSNwF-L9IraNsfmDiCAl7lHO5zVs1WRgl_nRYWRNx9TYO4DHJwNuNnXMLPn21HyBE6qwuOab2r0jM'
    })

    const calendar = google.calendar({
        version: 'v3',
        auth: oAuth2Client
    })

    const eventStartTime = new Date()
    eventStartTime.setFullYear(Number(year))
    eventStartTime.setMonth(Number(month) - 1)
    eventStartTime.setDate(Number(day))
    eventStartTime.setHours(Number(begin_hours))
    eventStartTime.setMinutes(Number(begin_minutes))


    const eventEndTime = new Date()
    eventEndTime.setFullYear(Number(year))
    eventEndTime.setMonth(Number(month) - 1)
    eventEndTime.setDate(Number(day))
    eventEndTime.setHours(Number(end_hours))
    eventEndTime.setMinutes(Number(end_minutes))


    const event = {
        summary: 'Novo Agendamento',
        location: 'Belém/PA',
        description: `Novo agendamento com ${name}.\nEmail: ${clientEmail}`, 
        start: {
            dateTime: eventStartTime,
            timeZone: 'America/Belem'
        },
        end: {
            dateTime: eventEndTime,
            timeZone: 'America/Belem'
        },
        ColorId: 1
    }

    const freebusyOptions: any = {
        resource: {
            timeMin: eventStartTime,
            timeMax: eventEndTime,
            timeZone: 'America/Belem',
            items: [{
                id: 'primary'
            }]
        }
    }

    calendar.freebusy.query(
        freebusyOptions
        ,
        (err: any, res: any) => {
            if (err) return console.error('Free Busy Query Error: ', err)

            const eventsArr = res.data.calendars.primary.busy

            if (eventsArr.length === 0) {
                return calendar.events.insert({
                    calendarId: email,
                    resource: event
                }, (err: any) => {
                    if (err) {
                        return console.error('Calendar Event Creation Error: ', err)
                    }

                    return console.log('Calendar Event Created.')
                })
            }

            return console.log('Busy')
        }
    )
}

export default googleCalendar