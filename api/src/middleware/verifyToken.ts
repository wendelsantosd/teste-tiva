import { Response, NextFunction } from 'express'
import { verify } from 'jsonwebtoken'
import { CustomRequest } from 'types'

import { config } from '../config'

const verifyToken = (
    req: CustomRequest,
    res: Response,
    next: NextFunction
): void => {
    const token: string | undefined = req.headers.authorization?.split(' ')[1]

    if (token) {
        const { secret } = config.JWT

        verify(token, secret, (err, payload) => {
            if (err) {
                res.status(401).json({error: err.message})
            } else {
                req.user = payload
                next()
            }
        })
    } else {
        res.status(401).json({error: 'missing token'})
    }
}

export { verifyToken }