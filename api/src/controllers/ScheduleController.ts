import { Response } from 'express'

import { schedules } from '@prisma/client'

import { prisma } from '../database'
import googleCalendar from '../services/googleCalendar'
import sendEmail from '../services/sendEMail'
import { CustomRequest } from '../types'


const scheduleController = {
    create: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const newSchedule: schedules = { ...req.body }

                if (newSchedule) {
                    newSchedule.created_at = new Date()
                    newSchedule.updated_at = new Date()

                    await prisma.schedules.create({
                        data: newSchedule
                    })

                    const professional = await prisma.users.findUnique({
                        where: {
                            id: newSchedule.user_id
                        }
                    })

                    await sendEmail(
                        professional?.email,
                        newSchedule.name,
                        newSchedule.scheduled_day,
                        newSchedule.begin_hour,
                        newSchedule.email
                    )

                    await googleCalendar(
                        newSchedule.scheduled_day,
                        newSchedule.begin_hour,
                        newSchedule.end_hour,
                        newSchedule.name,
                        newSchedule.email,
                        professional?.email
                    )

                    res.status(201).json({ message: 'schedule created' })
                } else {
                    res.status(412).json({ message: 'missing arguments' })
                }
            } else {
                res.status(403).json({ message: 'only admin can access'})
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    list: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const schedules = await prisma.schedules.findMany()

                res.status(200).json(schedules)
            } else {
                res.status(403).json({ message: 'only admin can access' })
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    data: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const targetScheduleId = req.body.id

                if (targetScheduleId) {
                    const targetSchedule = await prisma.schedules.findUnique({
                        where: {
                            id: targetScheduleId
                        }
                    })
                    
                    if (targetSchedule) {
                        res.status(200).json(targetSchedule)
                    } else {
                        res.status(404).json({ message: 'target schedule not found'})
                    }
                } else {
                    res.status(412).json({ message: 'missing arguments' })
                }
            } else {
                res.status(403).json({ message: 'only admin can access'})
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    update: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const targetScheduleId = req.body.id

                if (targetScheduleId) {
                    const targetSchedule = await prisma.schedules.findUnique({
                        where: {
                            id: targetScheduleId
                        }
                    })

                    if (targetSchedule) {
                        const body = {
                            ...req.body,
                            updated_at: new Date()
                        }

                        await prisma.schedules.update({
                            where: {
                                id: targetScheduleId
                            },
                            data: body
                        })

                        res.status(200).json({ message: 'schedule updated' })
                    } else {
                        res.status(404).json({ message: 'target schedule not found' })
                    }
                } else {
                    res.status(412).json({ message: 'missing arguments' })
                }
            } else {
                res.status(403).json({ message: 'only admin can access' })
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    delete: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const targetScheduleId = req.body.id

                if (targetScheduleId) {
                    const targetSchedule = await prisma.schedules.findUnique({
                        where: {
                            id: targetScheduleId
                        }
                    })

                    if (targetSchedule) {
                        await prisma.schedules.delete({
                            where: {
                                id: targetScheduleId
                            }
                        })

                        res.status(200).json({ message: 'schedule deleted' })
                    }  else {
                        res.status(404).json({ message: 'target schedule not found'})
                    }
                } else {
                    res.status(412).json({ message: 'missing arguments' })
                }
            } else {
                res.status(403).json({ message: 'only admin can access' })
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    }
}

export { scheduleController }