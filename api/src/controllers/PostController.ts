import { Response} from 'express'
import { CustomRequestPost } from 'types'

import { prisma } from '../database'

const postController: any = {
    create: async (req: CustomRequestPost, res: Response): Promise<void> => {
        try {
            const { user } = req
            
            if (user?.access_level === 'admin') {
                const {
                    originalname: name,
                    size,
                    key,
                    location: url = ''
                } = req.file

                const post = await prisma.posts.create({
                    data: {
                        name,
                        size,
                        key,
                        url,
                        user_id: user.id,
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                })

                res.status(201).json(post)   
            } else {
                res.status(403).json({ message: 'only admin can access'})
            }
        } catch (err: any) {
            res.status(500).json({ error: err.message})
        }
    },

    list: async (req: Request, res: Response): Promise<void> => {
        try {
            const posts = await prisma.posts.findMany()

            res.status(200).json(posts)
        } catch (err: any) {
            res.status(500).json({ message: err.message})
        }
    },

    delete: async (req: CustomRequestPost, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const targetPostKey = req.body.key

                if (targetPostKey) {
                      
                    const targetPost = await prisma.posts.findUnique({
                        where: {
                            key: targetPostKey
                        }
                    })

                    if (targetPost) {
                        await prisma.posts.delete({
                            where: {
                                key: targetPostKey
                            }
                        })

                        res.status(200).json({ message: 'post deleted' })
                    } else {
                        res.status(404).json({ message: 'target post not found' })
                    }
                } else {
                    res.status(412).json({ message: 'missing arguments'})
                }
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    }
}

export { postController }