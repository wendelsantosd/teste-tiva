import { hash, compare } from 'bcrypt'
import { Request, Response } from 'express'
import { sign } from 'jsonwebtoken'
import { CustomRequest } from 'types'

import { users } from '@prisma/client'

import { config } from '../config'
import { prisma } from '../database'


const userController = {
    auth: async (req: Request, res: Response): Promise<void> => {
        try {
            const { email, password } = req.query

            if (email && password) {
                const user = await prisma.users.findUnique({
                    where: {
                        email: String(email)
                    }
                })

                if (user) {
                    const isValid = await compare(
                        String(password) , 
                        user.hashed_password
                    )

                    if (isValid) {
                        const { secret, expTime } = config.JWT

                        const token = sign(
                            {
                                id: user.id,
                                access_level: user.access_level
                            },
                            secret,
                            {
                                expiresIn: expTime
                            }
                        )

                        res.status(200).json({ 
                            token: token,
                            id: user.id  
                        })
                    } else {
                        res.status(401).json({ message: 'email or password incorrect'})
                    }
                } else {
                    res.status(404).json({ message: 'email not found'})
                }
            } else {
                res.status(412).json({ message: 'missing arguments'})
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    firstUser: async (req: Request, res: Response): Promise<void> => {
        try {
            const newUser = { 
                ...req.body,
                created_at: new Date(),
                updated_at: new Date()
            }

            const password = newUser.hashed_password

            const hashPassword = await hash(password, 8)

            newUser.hashed_password = hashPassword

            delete newUser.address
            delete newUser.available_schedule

            const userCreated = await prisma.users.create({
                data: newUser
            })

            if (userCreated && req.body.address && req.body.available_schedule) {
                const address = {
                    user_id: userCreated.id,
                    zip: req.body.address.zip,
                    state: req.body.address.state,
                    city: req.body.address.city,
                    district: req.body.address.district,
                    street: req.body.address.street,
                    number: req.body.address.number,
                    created_at: new Date(),
                    updated_at: new Date()
                }

                const available_schedule = {
                    user_id: userCreated.id,
                    begin_date: req.body.available_schedule.begin_date,
                    end_date: req.body.available_schedule.end_date,
                    begin_hour: req.body.available_schedule.begin_hour,
                    end_hour: req.body.available_schedule.end_hour,
                    created_at: new Date(),
                    updated_at: new Date()
                }

                await prisma.addresses.create({
                    data: address
                })

                await prisma.available_schedules.create({
                    data: available_schedule
                })

                res.status(201).json({ 
                    message: 'user created'
                })
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    create: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const newUser: users = { 
                    ...req.body
                }

                const password = newUser.hashed_password

                const hashPassword = await hash(password, 8)

                newUser.hashed_password = hashPassword
                newUser.created_at = new Date()
                newUser.updated_at = new Date()


                await prisma.users.create({
                    data: newUser
                })

                res.status(201).json({ 
                    message: 'user created'
                })
            } else {
                res.status(401).json({ message: 'only admin can access'})
            }
           
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    list: async (req: Request, res: Response): Promise<void> => {
        try {
            const users = await prisma.users.findMany({
                select: {
                    id: true,
                    name: true,
                    access_level: true,
                    phone: true,
                    email: true,
                    description: true
                }
            })

            const addresses = await prisma.addresses.findMany({
                select: {
                    id: true,
                    user_id: true,
                    zip: true,
                    state: true,
                    city: true,
                    district: true,
                    street: true,
                    number: true
                }
            })

            const available_schedules = await prisma.available_schedules.findMany({
                select: {
                    id: true,
                    user_id: true,
                    begin_date: true,
                    end_date: true,
                    begin_hour: true,
                    end_hour: true
                }
            })

            const posts = await prisma.posts.findMany({
                select: {
                    key: true,
                    name: true,
                    size: true,
                    url: true,
                    user_id: true
                }
            })

            res.status(200).json({
                users, 
                addresses,
                available_schedules,
                posts
            })
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    data: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const targetUserId = req.query.id

            if(targetUserId) {
                const targetUser = await prisma.users.findUnique({
                    where: {
                        id: targetUserId
                    },
                    select: {
                        id: true,
                        name: true,
                        access_level: true,
                        phone: true,
                        email: true,
                        description: true
                    }
                })

                if(targetUser) {
                    const address = await prisma.addresses.findMany({
                        where: {
                            user_id: targetUserId
                        }
                    })

                    const available_schedules = await prisma.available_schedules.findMany({
                        where: {
                            user_id: targetUserId
                        }
                    })

                    const post = await prisma.posts.findMany({
                        where: {
                            user_id: targetUserId
                        }
                    })

                    res.status(200).json({
                        targetUser,
                        address,
                        available_schedules,
                        post
                    })
                } else {
                    res.status(404).json({ message: 'target user not found'})
                }
            } else {
                res.status(412).json({ message: 'missing arguments' })
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    update: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const targetUserId = req.body.id
                
                if (targetUserId) {
                    const targetUser = await prisma.users.findUnique({
                        where: {
                            id: targetUserId
                        }
                    })

                    if(targetUser) {
                        const body = { ...req.body }

                        const password = body.hashed_password

                        const hashPassword = await hash(password, 8)

                        body.hashed_password = hashPassword
                
                        body.updated_at = new Date()

                        await prisma.users.update({
                            where: {
                                id: targetUser.id
                            },
                            data: body
                        })

                        res.status(200).json({ 
                            message: 'user updated'
                        })
                    } else {
                        res.status(404).json({ message: 'target user not found' })
                    }
                } else {
                    res.status(412).json({ message: 'missing arguments' })
                }
            } else {
                res.status(403).json({ message: 'only admin can access' })
            }
           
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    delete: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req
            

            if (user.access_level === 'admin') {
                const targetUserId = req.body.id

                if(targetUserId) {
                    const targetUser = await prisma.users.findUnique({
                        where: {
                            id : targetUserId
                        }
                    })

                    if (targetUser) {
                        await prisma.users.delete({
                            where: {
                                id: targetUserId
                            }
                        })

                        res.status(200).json({ message: 'user deleted'})
                    } else {
                        res.status(404).json({ message: 'target user not found'})
                    }

                } else {
                    res.status(412).json({ message: 'missing arguments' })
                }
            } else {
                res.status(403).json({ message: 'only admin can access'})
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    }
}

export { userController }
