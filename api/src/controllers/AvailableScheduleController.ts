import { Response } from 'express'
import { CustomRequest } from 'types'

import { available_schedules } from '@prisma/client'

import { prisma } from '../database'

const availableScheduleController = { 
    create: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req
            if (user?.access_level === 'admin') {
                const newAvailableSchedule: available_schedules = {
                    ...req.body
                }
                if(newAvailableSchedule) {
                    newAvailableSchedule.created_at = new Date()
                    newAvailableSchedule.updated_at = new Date()

                    const targetUserId = newAvailableSchedule.user_id

                    const targetUser = await prisma.users.findUnique({
                        where: {
                            id: targetUserId
                        }
                    })

                    if (targetUser) {
                   
                        await prisma.available_schedules.create({
                            data: newAvailableSchedule
                        })

                        res.status(201).json({ message: 'available schedule created' })
                    } else {
                        res.status(404).json({ message: 'user not found to link'})
                    }
                } else {
                    res.status(412).json({ message: 'missing arguments' })
                }
            } else {
                res.status(403).json({ message: 'only admin can access' })   
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    list: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const availableSchedules = await prisma.available_schedules.findMany()

                res.status(200).json(availableSchedules)
            } else {
                res.status(403).json({ message: 'only admin can access' })
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    data: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const targetAvailableScheduleId = req.body.id

                if (targetAvailableScheduleId) {
                    const targetAvailableSchedule = await prisma.available_schedules.findUnique({
                        where: {
                            id: targetAvailableScheduleId
                        }
                    })
                    if (targetAvailableSchedule) {
                        res.status(200).json(targetAvailableSchedule)
                    } else {
                        res.status(404).json({ message: 'target available schedule no found'})
                    }
                } else {
                    res.status(412).json({ message: 'missing arguments' })
                }

            } else {
                res.status(403).json({ message: 'only admin can access' })
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    update: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const targetAvailableScheduleId = req.body.id

                if (targetAvailableScheduleId) {
                    const targetAvailableSchedule = await prisma.available_schedules.findUnique({
                        where: {
                            id: targetAvailableScheduleId
                        }
                    })

                    if (targetAvailableSchedule) {
                        const body = { 
                            ...req.body,
                            updated_at: new Date()
                        }
                        
                        await prisma.available_schedules.update({
                            where: {
                                id: targetAvailableScheduleId
                            },
                            data: body
                        })

                        res.status(200).json({ message: 'available schedule updated' })
                    } else {
                        res.status(404).json({ message: 'target available schedule not found' })
                    }
                } else {
                    res.status(412).json({ message: 'missing arguments' })
                }
            } else {
                res.status(403).json({ message: 'only admin can access' })
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    },

    delete: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if (user?.access_level === 'admin') {
                const targetAvailableScheduleId = req.body.id

                if (targetAvailableScheduleId) {
                    const targetAvailableSchedule = await prisma.available_schedules.findUnique({
                        where: {
                            id: targetAvailableScheduleId
                        }
                    })

                    if (targetAvailableSchedule) {
                        await prisma.available_schedules.delete({
                            where: {
                                id: targetAvailableScheduleId
                            }
                        })

                        res.status(200).json({ message: 'target available schedule deleted' })
                    } else {
                        res.status(404).json({ message: 'target available schedule not found' })
                    }
                } else {
                    res.status(412).json({ message: 'missing arguments' })
                }
            } else {
                res.status(403).json({ message: 'only admin can access' })
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message })
        }
    }
}

export { availableScheduleController }