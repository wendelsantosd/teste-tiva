import { Response } from 'express'
import { CustomRequest, CustomRequestAddress } from 'types'

import { addresses } from '@prisma/client'

import { prisma } from '../database'
const addressController = {
    create: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const { user } = req

            if ( user?.access_level === 'admin') {
                const newAddress: addresses = {
                    ...req.body,
                    created_at: new Date(),
                    updated_at: new Date()
                }

                if ( newAddress ) {
                    const targetUserId = newAddress.user_id

                    const targetUser = await prisma.users.findUnique({
                        where: {
                            id: targetUserId
                        }
                    })

                    if(targetUser) {

                        await prisma.addresses.create({
                            data: newAddress
                        })

                        res.status(201).json({ message: 'address created'})
                    } else {
                        res.status(404).json({ message: 'user not found to link' })
                    }
                } else {
                    res.status(412).json({ message: 'missing arguments' })
                } 
            } else {
                res.status(403).json({ message: 'only admin can access'})
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message})
        }
    },

    list: async (req: CustomRequest, res: Response): Promise<void> => {
        try {
            const addresses: addresses[] = await prisma.addresses.findMany()

            res.status(200).json(addresses)
        } catch (err: any) {
            res.status(500).json({ message: err.message})
        }
    },

    data: async (req: CustomRequestAddress, res: Response): Promise<void> => {
        try {
            const targetAddressId = req.body.id

            if (targetAddressId) {
                const targetAddress = await prisma.addresses.findUnique({
                    where: {
                        id: targetAddressId
                    }
                })

                if (targetAddress) {
                    res.status(200).json(targetAddress)
                }
                else {
                    res.status(404).json({message: 'target address not found'})
                }
            } else {
                res.status(412).json({ message: 'missing arguments'})
            }
            
        } catch (err: any) {
            res.status(500).json({ message: err.message})
        }
    },

    update: async (req: CustomRequestAddress, res: Response): Promise<void> => {
        try {
            const targetAddressId = req.body.id
            
            if (targetAddressId) {
                const targetAddress = await prisma.addresses.findUnique({
                    where: {
                        id: targetAddressId
                    }
                })

                if (targetAddress) {
                    const body = { 
                        ...req.body,
                        updated_at: new Date()
                    }

                    await prisma.addresses.update({
                        where: {
                            id: targetAddressId
                        },
                        data: body
                    })

                    res.status(200).json({ message: 'address updated' })
                } else {
                    res.status(404).json({ message: 'user not found'})
                }
            } else {
                res.status(412).json({ message: 'missing arguments'})
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message})
        }
    },

    delete: async (req: CustomRequestAddress, res: Response): Promise<void> => {
        try {
            const targetAddressId = req.body.id

            if (targetAddressId) {
                const targetAddress = await prisma.addresses.findUnique({
                    where: {
                        id: targetAddressId
                    }
                })

                if (targetAddress) {
                    await prisma.addresses.delete({
                        where: {
                            id: targetAddressId
                        }
                    })

                    res.status(200).json({ message: 'address deleted' })
                } else {
                    res.status(404).json({ message: 'target addresses nor found'})
                }
            } else {
                res.status(412).json({ message: 'missing arguments' })
            }
        } catch (err: any) {
            res.status(500).json({ message: err.message})
        }
    }
}

export { addressController }