/*
  Warnings:

  - You are about to drop the `schedules` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "schedules" DROP CONSTRAINT "schedules_user_id_fkey";

-- DropTable
DROP TABLE "schedules";

-- CreateTable
CREATE TABLE "available_schedules" (
    "id" TEXT NOT NULL,
    "begin_date" VARCHAR(255) NOT NULL,
    "end_date" VARCHAR(255) NOT NULL,
    "begin_hour" VARCHAR(255) NOT NULL,
    "end_hour" VARCHAR(255) NOT NULL,
    "user_id" TEXT NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL,
    "updated_at" TIMESTAMPTZ(6) NOT NULL,

    PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "available_schedules" ADD FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;
