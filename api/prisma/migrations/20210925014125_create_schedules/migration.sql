-- CreateTable
CREATE TABLE "schedules" (
    "id" TEXT NOT NULL,
    "begin_date" VARCHAR(255) NOT NULL,
    "end_date" VARCHAR(255) NOT NULL,
    "begin_hour" VARCHAR(255) NOT NULL,
    "end_hour" VARCHAR(255) NOT NULL,
    "user_id" TEXT NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL,
    "updated_at" TIMESTAMPTZ(6) NOT NULL,

    PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "schedules" ADD FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;
