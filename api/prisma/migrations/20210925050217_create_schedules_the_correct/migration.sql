-- CreateTable
CREATE TABLE "schedules" (
    "id" TEXT NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "phone" VARCHAR(255) NOT NULL,
    "scheduled_day" VARCHAR(255) NOT NULL,
    "begin_hour" VARCHAR(255) NOT NULL,
    "end_hour" VARCHAR(255) NOT NULL,
    "user_id" TEXT NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL,
    "updated_at" TIMESTAMPTZ(6) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "schedules.begin_hour_unique" ON "schedules"("begin_hour");

-- CreateIndex
CREATE UNIQUE INDEX "schedules.end_hour_unique" ON "schedules"("end_hour");

-- AddForeignKey
ALTER TABLE "schedules" ADD FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;
