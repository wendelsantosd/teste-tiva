# FRONTEND TEST TIVA REACT JS

Implementado usando React JS, esta plataforma consome a API que se encontra neste mesmo repositório.

Implementa apenas algumas funcionalidades como visualizar o catálogo de profissionais, login, e uma página onde todas as informações ligadas ao usuário autenticado são fornecidas.

---

### Instalação

3. Instale as dependências (Precisa do Node):

```shell
    npm install
```

ou

```shell
    yarn
```

4. Execute o projeto:

```shell
    npm run dev
```

ou

```shell
    yarn dev
```
