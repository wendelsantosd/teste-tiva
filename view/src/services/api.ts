import axios from 'axios'
import storage from './storage'

const baseURL = 'http://localhost:3333/'

const axiosInstance = axios.create({
    baseURL
})

axiosInstance.interceptors.request.use( async config => {
    const token = storage.read('JWT')

    if (storage.read(token)) {
        config.headers.authorization = `Bearer ${token}`
    }
    return config
})

const api = {
    getAllUsersData: async (): Promise<string | undefined> => {
        try {
            const response = await axiosInstance.get('user/list')
            const { status, data } = response

            if(status === 200) {
                return await data
            } else {
                return 'Ocorreu um erro'
            }
        } catch (err: any) {
            if (err?.response?.data?.message) {
                console.log(`ERRO NO SERVIDOR: ${
                err?.response?.data?.message
            }`)
            } else if (err?.message) {
                console.log(`ERRO LOCAL: ${
                err?.message
            }`)
            } else {
                console.log('ERRO NÃO IDENTIFICADO')
            }
            return 'Ocorreu um erro'
        }
    },
    getUserData: async (id: string | undefined): Promise<string | undefined> => {
        try {
            const response = await axiosInstance.get(`user/data?id=${id}`)
            const { status, data } = response

            const { targetUser, post, address, available_schedules } = data
            if(status === 200) {
                storage.write('user', targetUser)
                storage.write('post', post)
                storage.write('address', address)
                storage.write('available-schedule', available_schedules)
            } else {
                return 'Ocorreu um erro'
            }
        } catch (err: any) {
            if (err?.response?.data?.message) {
                console.log(`ERRO NO SERVIDOR: ${
                err?.response?.data?.message
            }`)
            } else if (err?.message) {
                console.log(`ERRO LOCAL: ${
                err?.message
            }`)
            } else {
                console.log('ERRO NÃO IDENTIFICADO')
            }
            return 'Ocorreu um erro'
        }
    },

    login: async (email: string | undefined, password: string | undefined): Promise<string | undefined> => {
        try {
            const response = await axiosInstance.get(
            `user/auth?email=${email}&password=${password}`
            )
            const {status, data } = response
            const { token } = data
            const { id } = data

            if (status === 200) {
                storage.clear('all')
                await api.getUserData(id)
                storage.write('JWT', token)
                storage.write('userId', id)
                return 'OK'

            } else {
                return 'Ocorreu um erro'
            }
        } catch (err: any) {
            if (err?.response?.data?.message) {
                if (err?.response?.data?.message === 'jwt expired') {
                    window.location.replace(
                    `${window.location.href.split('#')[0]}#/login`
                    )
                }

                console.log(`ERRO NO SERVIDOR: ${
                err?.response?.data?.message
            }`)
            } else if (err?.message) {
                console.log(`ERRO LOCAL: ${
                err?.message
            }`)
            } else {
                console.log('ERRO NÃO IDENTIFICADO')
            }

            const status = err?.response?.status

            if (status === 404) {
                return 'Usuário não encontrado'
            } else if (status === 401) {
                return 'Senha incorreta'
            } else {
                return 'Ocorreu um erro'
            }
        }
    }
}


export default api