import { RouteComponentProps } from 'react-router'

type HeaderComponent = RouteComponentProps & {
    isAuthenticated: boolean
    text?: string
    logout?: () => void
}

type User = & {
    id?: string
    name: string
    access_level: string
    hashed_password?: string
    phone?: string
    email: string
    description?: string
}

type Address = & {
    id?: string
    user_id?: string
    zip?: string
    state?: string
    city?: string
    district?: string
    street?: string
    number?: string
}

type AvailableSchedule = & {
    id?: string
    user_id?: string
    begin_date?: string
    end_date?: string
    begin_hour?: string
    end_hour?: string
}

type Post = & {
    key: string
    user_id: string
    name: string
    size: number
    url: string
}

type UserFullData = User & Address & AvailableSchedule & Post

export {
    HeaderComponent,
    User,
    Address,
    AvailableSchedule,
    Post,
    UserFullData
}