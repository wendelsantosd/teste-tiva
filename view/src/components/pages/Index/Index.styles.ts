import styled from 'styled-components'

const master = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100vw;
`

const main = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    background: var(--color-primary);
`

const separator = styled.div`
    height: 1px;
    background-color: var(--color-hairline-in-dark);
    margin: 1rem 0;
    width: 60%;
`

const container = styled.div`
    display: flex;
    justify-content: space-between;
    height: 9rem;
    width: 60%;
    background: var(--color-primary);
    transition: all 0.2s;

    .available-pros {
        display: flex;
        flex-direction: column;
        align-items: left;
        font-weight: 600;

        p {
            color: var(--color-text);

            &:nth-child(1) {
                font-size: 1.5rem;
            }
        }
    }
`

const login = styled.form`

    label {
        color: var(--color-text);
        margin: 0.2rem;
        font-size: 0.8rem;
    }

    input {
        width: 13rem;
        height: 1.5rem;
        border-radius: 0.3rem;
        border: none;
        padding: 0.2rem;
        font-size: 0.8rem;
    }
`

const pros = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    /* height: auto; */
    margin: 0rem auto 0rem auto;
    background-color: transparent;
    margin-top: -4rem;
    transition: all 0.2s;
`

const pro = styled.div`
    display: grid;
    grid-template-columns: 15% 30% 35% 20%;
    align-items: center;
    justify-items: center;
    height: 10rem;
    width: 60%;
    margin-bottom: 0.5rem;
    position: relative;
    background: var(--color-card-background);
    border-radius: 0.5rem;

    transition: all 0.2s;

    &:hover {
         background: linear-gradient(
            90deg,
            rgba(250, 156, 45, 0.1) 0.45%,
            rgba(252, 253, 255, 0.1) 31.4%
        ),
        white;
    }

    img {
        width: 6rem; /* 64px of 16px root*/
        height: 6rem; /* 64px of 16px root*/
        border-radius: 50%;
        /* border: 0.156rem solid #f1972c; */
        margin-left: 1rem;
    }

    div {
        display: flex;
        flex-direction: column;
    }

    p {
        color: var(--color-card-title);
        font-weight: 600;
    }

    .name {
        font-size: 1.5rem;
    }
`

export default {
    master,
    main,
    container,
    login,
    separator,
    pros,
    pro
}
