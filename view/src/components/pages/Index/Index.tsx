import React, { useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router'
import api from '../../../services/api'

import { User, Post, AvailableSchedule, UserFullData } from '../../../types'
import Header from '../../blocks/Header'

import styles from './Index.styles'


const Index: React.FC<RouteComponentProps> = ({ history }) => {
    const [usersData, setUsersData] = useState<User[]>()
    const [availableSchedulesData, setAvailableSchedulesData] = useState<AvailableSchedule[]>()
    const [postsData, setPostsData] = useState<Post[]>()
    const [email, setEmail] = useState<string>()
    const [password, setPassword] = useState<string>()

    useEffect(() => {
        (async () => {
            const data: any = await api.getAllUsersData()
            const { users, available_schedules, posts } = data

            if (data) {
                setUsersData(users)
                setAvailableSchedulesData(available_schedules)
                setPostsData(posts)
            }

        })()
    }, [])

    const submit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()

        const result = await api.login(email, password)
        console.log(result)

        if (result === 'OK') {
            history.push('/home')
        }
    }
    return <>
        <Header
            isAuthenticated={false}
        />
        <styles.master>
            <styles.main>
                <styles.separator></styles.separator>
                <styles.container
                    className='animate-up'
                >
                    <div
                        className='available-pros'
                    >
                        <p>{usersData?.length}</p>
                        <p>Profissionais disponíveis</p>
                    </div>

                    <styles.login onSubmit={submit}>
                        <label htmlFor='email'>
                            E-mail:
                        </label>
                        <input
                            id='email'
                            onChange={event => {
                                setEmail(event.target.value)
                            }}
                        />

                        <label htmlFor='password'>
                            Senha:
                        </label>
                        <input
                            id='password'
                            type='password'
                            onChange={event => {
                                setPassword(event.target.value)
                            }}
                        />

                        <button
                            type='submit'
                            className='button orange'
                        >
                            Login
                        </button>
                    </styles.login>
                </styles.container>
            </styles.main>
            <styles.pros
                className='animate-up'
            >
                {
                    usersData?.map(user => <styles.pro key={user.id}>
                        {postsData?.map(post => {
                            if (post.user_id === user.id) {
                                return <img src={post.url} />
                            } else {
                                return <img src='https://www.filtersol.com.br/img/vendedores/foto-vendedor-padrao.png' />
                            }
                        })}

                        { postsData?.length === 0 ?
                            <img src='https://www.filtersol.com.br/img/vendedores/foto-vendedor-padrao.png' />
                            : null
                        }

                        <p
                            className='name'
                        >
                            {user.name}
                        </p>
                        <div>
                            <p>Horários disponíveis</p>
                            {availableSchedulesData?.map(availableSchedule => {
                                if (availableSchedule.user_id == user.id) {
                                    return <p
                                        key={availableSchedule.id}
                                    >
                                        {availableSchedule.begin_date} - {availableSchedule.begin_hour}hs às {availableSchedule.end_hour}hs
                                    </p>
                                }
                            }
                            ) }
                        </div>
                        <button
                            className='button green'
                        >
                            Agendar
                        </button>
                    </styles.pro>)
                }
            </styles.pros>
        </styles.master>
    </>
}

export default Index