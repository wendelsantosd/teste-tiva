import styled from 'styled-components'

const master = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100vw;
`

const main = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    background: var(--color-primary);
`

const separator = styled.div`
    height: 1px;
    background-color: var(--color-hairline-in-dark);
    margin: 1rem 0;
    width: 60%;
`

const container = styled.div`
    display: flex;
    justify-content: space-between;
    height: 9rem;
    width: 60%;
    background: var(--color-primary);
    transition: all 0.2s;
     
    p {
        color: var(--color-text);
        font-size: 1.5rem;
    }
`

const login = styled.form`

    label {
        color: var(--color-text);
        margin: 0.2rem;
        font-size: 0.8rem;
    }

    input {
        width: 13rem;
        height: 1.5rem;
        border-radius: 0.3rem;
        border: none;
        padding: 0.2rem;
        font-size: 0.8rem;
    }
`

const pros = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    /* height: auto; */
    margin: 0rem auto 0rem auto;
    background-color: transparent;
    margin-top: -4rem;
    transition: all 0.2s;
`

const profile = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1rem;
    width: 60%;
    margin-bottom: 0.5rem;
    position: relative;
    background: var(--color-card-background);
    border-radius: 0.5rem;

    input, select {
        border: none;
        background: var(--color-card-background);
        margin-bottom: 1rem;
    }

    .edit-profile {
        border: 1px solid #777;
        border-radius: 0.3rem;
        width: 20rem;
        background: var(--color-card-background);
        margin-bottom: 1rem;
        padding: 0.2rem;
    }

    p {
        color: var(--color-card-title);
        font-weight: 600;
    }

    .title {
        color: var(--color-card-title);
        font-weight: 600;
        font-size: 1.4rem;
    }
`

export default {
    master,
    main,
    container,
    login,
    separator,
    pros,
    profile,
}
