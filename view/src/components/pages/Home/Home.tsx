import React, { useEffect, useState } from 'react'

import Header from '../../blocks/Header'

import storage from '../../../services/storage'

import styles from './Home.style'
import { Address, AvailableSchedule, User } from '../../../types'
import { RouteComponentProps } from 'react-router'

const Home: React.FC<RouteComponentProps> = ({ history }) => {
    const [user, setUser] = useState<User>()
    const [availableSchedule, setAvailableSchedule] = useState<AvailableSchedule[]>()
    const [address, setAddress] = useState<Address[]>()
    const [editPro, setEditPro] = useState<boolean>(false)
    const [editSchedule, setEditSchedule] = useState<boolean>(false)
    const [editAddress, setEditAddress] = useState<boolean>(false)

    useEffect(() => {
        const _user = storage.read('user')
        const _available_schedules = storage.read('available-schedule')
        const _address = storage.read('address')

        setUser(_user)
        setAvailableSchedule(_available_schedules)
        setAddress(_address)
    }, [])

    return <>
        <Header
            isAuthenticated={true}
            text='A plataforma ideal para o Profissional'
        />
        <styles.master>
            <styles.main>
                <styles.separator></styles.separator>
                <styles.container
                    className='animate-up'
                >
                    <p>Bem vindo a sua Dashboard.</p>
                </styles.container>
            </styles.main>
            <styles.pros
                className='animate-up'
            >

                <styles.profile>
                    <p className='title'>Dados do profissional</p>

                    <label htmlFor='name'>Nome</label>
                    <input
                        className={editPro ? 'edit-profile': 'profile' }
                        id='name'
                        defaultValue={user?.name}
                        readOnly={!editPro}
                    />


                    <label htmlFor='email'>E-mail</label>
                    <input
                        className={editPro ? 'edit-profile': 'profile' }
                        id='email'
                        defaultValue={user?.email}
                        readOnly={!editPro}
                    />

                    <label htmlFor='phone'>Telefone</label>
                    <input
                        className={editPro ? 'edit-profile': 'profile' }
                        id='phone'
                        defaultValue={user?.phone}
                        readOnly={!editPro}
                    />

                    <label htmlFor='access-level'>Nível de acesso</label>
                    <select
                        className={editPro ? 'edit-profile': 'profile' }
                        id='access-level'
                        defaultValue={user?.access_level}
                        disabled={!editPro}
                    >
                        <option value='admin'>Administrador</option>
                        <option value='client'>Profissional</option>
                    </select>

                    { !editPro ?
                        <button
                            className='button green'
                            onClick={event => {
                                event.preventDefault()
                                setEditPro(!editPro)
                            }}
                        >
                                Editar
                        </button>
                        :
                        <>
                            <button
                                className='button green'
                                onClick={event => {
                                    event.preventDefault()
                                    setEditPro(!editPro)
                                }}
                            >
                                Salvar
                            </button>
                            <button
                                className='button red'
                                onClick={event => {
                                    event.preventDefault()
                                    setEditPro(!editPro)
                                }}
                            >
                                Cancelar
                            </button>
                        </>
                    }
                </styles.profile>

                <styles.profile>
                    <p className='title'>Horários disponíveis</p>
                    {availableSchedule?.map((schedule, index) => {
                        return <>
                            <p>Horário {index+1}</p>
                            <label htmlFor='begin-date'>Data de Início</label>
                            <input
                                className={editSchedule ? 'edit-profile': 'profile' }
                                id='begin-date'
                                defaultValue={schedule.begin_date}
                                readOnly={!editSchedule}
                            />

                            <label htmlFor='begin-end'>Data Final</label>
                            <input
                                className={editSchedule ? 'edit-profile': 'profile' }
                                id='begin-end'
                                defaultValue={schedule.end_date}
                                readOnly={!editSchedule}
                            />

                            <label htmlFor='begin-hour'>Hora de Início</label>
                            <input
                                className={editSchedule ? 'edit-profile': 'profile' }
                                id='begin-hour'
                                defaultValue={schedule.begin_hour}
                                readOnly={!editSchedule}
                            />

                            <label htmlFor='end-hour'>Hora Final</label>
                            <input
                                className={editSchedule ? 'edit-profile': 'profile' }
                                id='end-hour'
                                defaultValue={schedule.end_hour}
                                readOnly={!editSchedule}
                            />
                        </>
                    })}

                    { !editSchedule ?
                        <>
                            <button
                                className='button green'
                                onClick={event => {
                                    event.preventDefault()
                                    setEditSchedule(!editSchedule)
                                }}
                            >
                                Editar
                            </button>
                            <button
                                className='button green'
                            >
                                Adicionar
                            </button>
                        </>
                        :
                        <>
                            <button
                                className='button green'
                                onClick={event => {
                                    event.preventDefault()
                                    setEditSchedule(!editSchedule)
                                }}
                            >
                                Salvar
                            </button>
                            <button
                                className='button red'
                                onClick={event => {
                                    event.preventDefault()
                                    setEditSchedule(!editSchedule)
                                }}
                            >
                                Cancelar
                            </button>
                        </>
                    }
                </styles.profile>

                <styles.profile>
                    <p className='title'>Endereços</p>
                    {address?.map((address, index) => {
                        return <>
                            <p>Endereço {index+1}</p>
                            <label htmlFor='street'>Rua</label>
                            <input
                                className={editAddress ? 'edit-profile': 'profile' }
                                id='street'
                                defaultValue={address.street}
                                readOnly={!editAddress}
                            />

                            <label htmlFor='number'>Número</label>
                            <input
                                className={editAddress ? 'edit-profile': 'profile' }
                                id='number'
                                defaultValue={address.number}
                                readOnly={!editAddress}
                            />

                            <label htmlFor='district'>Bairro</label>
                            <input
                                className={editAddress ? 'edit-profile': 'profile' }
                                id='district'
                                defaultValue={address.district}
                                readOnly={!editAddress}
                            />

                            <label htmlFor='city'>Cidade</label>
                            <input
                                className={editAddress ? 'edit-profile': 'profile' }
                                id='city'
                                defaultValue={address.city}
                                readOnly={!editAddress}
                            />

                            <label htmlFor='state'>Estado</label>
                            <input
                                className={editAddress ? 'edit-profile': 'profile' }
                                id='state'
                                defaultValue={address.state}
                                readOnly={!editAddress}
                            />
                        </>
                    })}

                    { !editAddress ?
                        <>
                            <button
                                className='button green'
                                onClick={event => {
                                    event.preventDefault()
                                    setEditAddress(!editAddress)
                                }}
                            >
                                Editar
                            </button>
                            <button
                                className='button green'
                            >
                                Adicionar
                            </button>
                        </>
                        :
                        <>
                            <button
                                className='button green'
                                onClick={event => {
                                    event.preventDefault()
                                    setEditAddress(!editAddress)
                                }}
                            >
                                Salvar
                            </button>
                            <button
                                className='button red'
                                onClick={event => {
                                    event.preventDefault()
                                    setEditAddress(!editAddress)
                                }}
                            >
                                Cancelar
                            </button>
                        </>
                    }
                </styles.profile>
            </styles.pros>
        </styles.master>

        <styles.main>

        </styles.main>
    </>
}

export default Home