import React from 'react'
import GlobalStyle from './App.styles'
import Routes from '../../routes'

const App: React.FC = () => <>
    <GlobalStyle />
    <Routes />
</>

export default App
