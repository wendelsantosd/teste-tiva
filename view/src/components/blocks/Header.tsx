import React, { useEffect, useState } from 'react'
import { HeaderComponent, User } from '../../types'
import { withRouter } from 'react-router-dom'
import storage from '../../services/storage'
import styles from './Header.style'

const Header: React.FC<HeaderComponent> = ({ history, isAuthenticated, text }) => {
    const [user, setUser] = useState<User>()
    const [post, setPost] = useState<any>()

    useEffect(() => {
        const _user = storage.read('user')
        const _post = storage.read('post')

        setUser(_user)
        setPost(_post)
    }, [])

    return <styles.main>
        <styles.container
            className="animate-up"
        >
            <p
                className='name-app'
            >
                ActivePro
            </p>


            <p>
                { text ?  text : 'Agende com um profissional'}
            </p>

            <styles.profile>
                { isAuthenticated ?
                    <>
                        <div >
                            <p className='name'>
                                {user?.name}
                            </p>

                            <a
                                onClick={() => {
                                    storage.clear('all')
                                    history.push('/login')
                                }}
                            >
                                Sair
                            </a>
                        </div>

                        { post !== undefined ?
                            user?.id === post[0]?.user_id ?
                                <img src={post[0].url} />
                                :
                                <img src='https://www.filtersol.com.br/img/vendedores/foto-vendedor-padrao.png' />
                            :
                            <img src='https://www.filtersol.com.br/img/vendedores/foto-vendedor-padrao.png' />
                        }
                    </>
                    : null
                }
            </styles.profile>
        </styles.container>
    </styles.main>
}
export default withRouter(Header)