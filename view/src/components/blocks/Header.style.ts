import styled from 'styled-components'

const main = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100vw;
    height: 7rem;
    background: var(--color-primary);

    p {
        color: var(--color-text);
    }


    .name-app {
        font-family: Poppins, sans-serif;
        font-weight: 600;
        font-size: 2rem;
        color: var(--color-text);
    }
`

const container = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 10rem;
    width: 60%;
`

const profile = styled.div `
    display: flex;
    align-items: center;
    justify-content: space-between;

    div {
        display: flex;
        flex-direction: column;

        .name {
        font-weight: 600;
        font-size: 1.2rem;
        color: var(--color-text);
    }

    a {
        text-align: right;
        color: var(--color-text);
        font-size: 0.8rem;
        cursor: pointer;
        transition: all 0.2s;

        &:hover {
            color: var(--color-secondary-hover);
        }
    }

    }

    img {
        width: 5rem; /* 64px of 16px root*/
        height: 5rem; /* 64px of 16px root*/
        border-radius: 50%;
        border: 0.156rem solid #f1972c;
        margin-left: 1rem;
    }
`

export default {
    main,
    container,
    profile
}