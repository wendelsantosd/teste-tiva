import React from 'react'
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom'

import Index from './components/pages/Index/Index'
import Home from './components/pages/Home/Home'

const Routes: React.FC = () => <Router>
    <Switch>
        <Route path='/index' exact component={Index}/>
        <Route path='/home' exact component={Home}/>
        <Redirect to='/index' />
    </Switch>
</Router>

export default Routes